    - orgId: 1
      name: {{ .Name }}
      folder: {{ .Folder }}
      interval: 1m
      rules:
        - uid: {{ .UID }}
          title: {{ .Alert }}
          condition: C
          data:
            - refId: A
              relativeTimeRange:
                from: 600
                to: 0
              datasourceUid: mimir
              model:
                editorMode: code
                expr: |
                  {{ .Expr }}
                hide: false
                intervalMs: 1000
                legendFormat: __auto
                maxDataPoints: 43200
                range: true
                refId: A
            - refId: B
              relativeTimeRange:
                from: 600
                to: 0
              datasourceUid: __expr__
              model:
                conditions:
                    - evaluator:
                        params: []
                        type: gt
                      operator:
                        type: and
                      query:
                        params:
                            - B
                      reducer:
                        params: []
                        type: last
                      type: query
                datasource:
                    type: __expr__
                    uid: __expr__
                expression: A
                hide: false
                intervalMs: 1000
                maxDataPoints: 43200
                reducer: last
                refId: B
                type: reduce
            - refId: C
              relativeTimeRange:
                from: 600
                to: 0
              datasourceUid: __expr__
              model:
                conditions:
                    - evaluator:
                        params:
                            - 0
                        type: gt
                      operator:
                        type: and
                      query:
                        params:
                            - C
                      reducer:
                        params: []
                        type: last
                      type: query
                datasource:
                    type: __expr__
                    uid: __expr__
                expression: B
                hide: false
                intervalMs: 1000
                maxDataPoints: 43200
                refId: C
                type: threshold
          noDataState: OK
          execErrState: Error
          for: {{or .For "0m"}}
          annotations:
            {{- range $key, $value := .Annotations }}
            {{ $key }}: |
              {{ $value }}
            {{- end }}
          labels:
            {{- range $key, $value := .Labels }}
            {{ $key }}: {{ $value }}
            {{- end }}
          isPaused: false
