package grafana_utils

import (
	"crypto/md5"
	"encoding/hex"
	logz "gitlab.com/navenest/grafana-util/pkg/logger"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
	"os"
	"strings"
	"text/template"
)

type PromAlerts struct {
	Groups []PromGroups `yaml:"groups"`
}

type PromGroups struct {
	Name  string      `yaml:"name"`
	Rules []PromRules `yaml:"rules"`
}

type PromRules struct {
	Alert       string            `yaml:"alert"`
	Annotations map[string]string `yaml:"annotations"`
	Expr        string            `yaml:"expr"`
	For         string            `yaml:"for"`
	Labels      map[string]string `yaml:"labels"`
}

type GrafanaAlert struct {
	Name        string
	Folder      string
	Alert       string
	Expr        string
	For         string
	Annotations map[string]string
	Labels      map[string]string
	UID         string
}

func Read() PromAlerts {
	file, err := os.ReadFile("examples/k8s-alerts.yaml")
	if err != nil {
		logz.Logger.Error("Unable to read file",
			zap.Error(err))
	}

	var data PromAlerts
	err = yaml.Unmarshal(file, &data)
	if err != nil {
		logz.Logger.Error("Unable to unmarshal yaml from file",
			zap.Error(err))
	}
	return data
}

func Output() {
	data := Read()
	temp, err := template.ParseFiles("internal/grafana_utils/grafana-alerts.tpl")
	if err != nil {
		logz.Logger.Error("Unable to parse template file",
			zap.Error(err))
	}
	file, err := os.Create("output.yaml")
	_, _ = file.WriteString("apiVersion: 1\ngroups:\n")
	for i, value := range data.Groups {

		for j, subValue := range value.Rules {
			currentRule := GrafanaAlert{
				Name:        value.Name,
				Folder:      "temp",
				Alert:       subValue.Alert,
				Expr:        strings.Replace(subValue.Expr, "\n", " ", -1),
				For:         subValue.For,
				Annotations: subValue.Annotations,
				Labels:      subValue.Labels,
			}
			hash := md5.Sum([]byte(currentRule.Name + currentRule.Folder + currentRule.Alert))
			uid := hex.EncodeToString(hash[:])
			currentRule.UID = uid
			err = temp.Execute(file, currentRule)
			if err != nil {
				logz.Logger.Error("Unable to execute template",
					zap.Error(err),
					zap.Int("index", i),
					zap.Int("subIndex", j))
			}
		}
	}
}
