package main

import (
	"gitlab.com/navenest/grafana-util/internal/grafana_utils"
	logz "gitlab.com/navenest/grafana-util/pkg/logger"
	"gitlab.com/navenest/grafana-util/web"
)

func main() {
	logz.Logger.Info("logger construction succeeded")
	grafana_utils.Output()
	webserver := web.App{}
	webserver.Initialize()
	webserver.Run(":8080")
}
