package web

import (
	"github.com/gin-gonic/gin"
	_ "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	logz "gitlab.com/navenest/grafana-util/pkg/logger"
	"go.uber.org/zap"
	"net/http"
	"time"
)

type App struct {
	Router *gin.Engine
}

type health struct {
	Status string `json:"status"`
}

func (web *App) middlewareLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path

		c.Next()
		latency := time.Now().Sub(start)

		logz.Logger.Info("Web Access Logs",
			zap.String("client_ip", c.ClientIP()),
			zap.String("method", c.Request.Method),
			zap.String("path", path),
			zap.Int("status_code", c.Writer.Status()),
			zap.Int("body_size", c.Writer.Size()),
			zap.Duration("latency", latency))
	}
}

func prometheusHandler() gin.HandlerFunc {
	h := promhttp.Handler()

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func (web *App) Initialize() {

	web.Router = gin.New()
	web.Router.Use(web.middlewareLogger())
	web.initializeRoutes()
}

func (web *App) initializeRoutes() {
	web.Router.GET("/metrics", prometheusHandler())
	web.Router.GET("/healthcheck", web.healthcheck)
}

func (web *App) healthcheck(c *gin.Context) {
	data := health{
		Status: "UP",
	}
	c.JSON(http.StatusOK, data)
}

func (web *App) Run(addr string) {
	if addr == "" {
		addr = ":8080"
	}
	err := web.Router.Run(addr)
	if err != nil {
		logz.Logger.Fatal("Issues with webserver: "+err.Error(),
			zap.Error(err))
	}
}
